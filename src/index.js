const express = require('express');
const colors = require('colors');;
const path = require('path');


//initializations 

const app = express();


//settings

app.set('port', process.env.PORT || 3000);



// middleware

app.use(express.urlencoded({
    extended: false
}));
app.use(express.json());


// Global Variables

app.use((request, response, next) => {
    next();
});

// Routes

app.use(require('./routes'));


app.use(express.static(path.join(__dirname, 'public')));


//Start Server

app.listen(app.get('port'), () => {
    console.log("Server on port ".green, app.get('port'));
});

app.set('close',()=>{
    app.close();
});

module.exports = app;