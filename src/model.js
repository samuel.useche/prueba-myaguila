'use strict';

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

function db() {

  
  this.hostMongo = '127.0.0.1';
  this.portMongo = '27017';
  this.databaseMongo = 'config';
  this.userMongo = 'admin';
  this.passwordMongo = 'admin123';
}


db.prototype.connectmongoDB = async function (){
 
const url = 'mongodb://'+this.hostMongo+':'+this.portMongo//+'/'+this.databaseMongo;
  //console.log(url);
  let client = await MongoClient.connect(url,{useNewUrlParser: true,useUnifiedTopology: true,auth:{user:this.userMongo,password: this.passwordMongo}});
  client.db(this.databaseMongo);
  return  client;
};

module.exports = db;
