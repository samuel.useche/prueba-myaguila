const express = require('express');
const router = express.Router();
const db = require('../model');

router.get('/viajes', async (request, response) => {
    let mongo = new db("dev");
    let conect = await mongo.connectmongoDB();
    const collection = conect.collection("viajes");
    // Find some documents
    let cantViajes = await collection.find({}).count();
    console.log(cantViajes);
    response.status(200).send({code: 100, data:"Numero de viajes "+cantViajes});
});

router.get('/viaje/ciudad/:idCiudad', async (request, response) => {
    let mongo = new db("dev");
    let conect = await mongo.connectmongoDB();
    const collection = conect.collection("viajes");
    let nombreCiudad = request.params.idCiudad;
    // Find some documents
    let cantViajes = await collection.find({'city.name': nombreCiudad}).count();
    console.log(cantViajes);
    response.status(200).send({code: 100, data:"Numero de viajes de la ciudad "+nombreCiudad+" es de "+cantViajes});
});

router.post('/crear-viaje', async (request, response) => {
    let payload = request.body;
    payload['createdAt']=new Date();
    payload['updateAt']=new Date();
    let mongo = new db("dev");
    let conect = await mongo.connectmongoDB();
    const collection = conect.collection("viajes");
    let cantViajes = await collection.insertMany([
        payload,
      ]);
    console.log(cantViajes);
    response.status(200).send({code: 100, data:"Insertado en el id "+cantViajes.insertedIds});
});

router.post('/update-viaje/:id', async (request, response) => {
    let payload = request.body;
    payload['updateAt']=new Date();
    let mongo = new db("dev");
    let conect = await mongo.connectmongoDB();
    const collection = conect.updateOne({ _id : request.params.id }
        , { $set: payload });
        response.status(200).send({code: 100, data:"Actualizado en el id "+cantViajes.insertedIds});
});

module.exports = router;