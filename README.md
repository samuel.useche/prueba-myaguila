# Mi Aguila-backend-test

### Requisitos minimos 

 * Nodejs v10.15.3
 * Mongo 
 * npm 6.4.1
 * Git



### Instrucciones de instalacion

 en terminal linux

 * Ejecutar -> $ git clone https://gitlab.com/samuel.useche/prueba-myaguila.git
 * Ejecutar -> $npm install
 * Pruebas -> $ npm run test

Listo ! puede abrir el proyecto en http://localhost:3000/
